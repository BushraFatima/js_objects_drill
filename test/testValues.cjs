// Importing modules.
let getValues = require ('../values.cjs');
// Given object.
const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
// Value function returns values array.
const valuesArray = getValues(testObject);
// Printing the result array.
console.log(valuesArray);