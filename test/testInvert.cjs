// Importing modules.
let doInvert = require ('../invert.cjs');
// Given object.
const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
// Function returning inverted ( keys --> values, values --> keys) object.
const invertObject = doInvert(testObject);
// Printing the inverted object.
console.log(invertObject);
