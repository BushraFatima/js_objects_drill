// Importing modules.
let mapObj = require ('../mapObject.cjs');
// Given object.
const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
// Function to apply on each property value.
function addValue(value, key) {

    if (isNaN(value)) {
        return value + ' Brook';

    } else {
        return value + 5;
    }
}
// Function returning maped object.
const newObject = mapObj(testObject, addValue);
// Printing the maped object.
console.log(newObject);
