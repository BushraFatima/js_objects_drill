// Importing modules.
let getPairs = require ('../pairs.cjs');
// Given object.
const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
// Pairs function returns pairs [key, value] array.
const pairsArray = getPairs(testObject);
// Printing the resultant array.
console.log(pairsArray);