// Importing modules.
let addDefault = require ('../defaults.cjs');
// Given object.
const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
// Function returning new object with default value if any.
const newObject = addDefault(testObject, {name: 'Bruce Wayne', age: 34, location: 'Gotham', role: 'Batman'});
// Printing the new object.
console.log(newObject);
