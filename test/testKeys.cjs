// Importing modules.
let getKeys = require ('../keys.cjs');
// Given object.
const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
// Key function returns keys array.
const keysArray = getKeys(testObject);
// Printing the resultant array.
console.log(keysArray);
