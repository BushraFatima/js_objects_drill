function getKeys(obj) {
    
    if (typeof obj === 'object' && obj !== {}){
        let keys = [];

        for (data in obj) {
            keys.push(data);
        }
    
        return keys;

    } else {
        return [];
    }

}

module.exports = getKeys;