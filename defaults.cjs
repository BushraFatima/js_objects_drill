function defaults(obj, defaultObj) {

    if (typeof obj === 'object' && obj !== {}){
         let newObj = {};

        for (data in defaultObj) {

            if (data in obj) {
                newObj[data] = obj[data];

            } else {
                newObj[data] = defaultObj[data];
            }
            
        }
    
        return newObj;

    } else {
        return {};
    }
    
}

module.exports = defaults;