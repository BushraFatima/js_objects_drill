function getValues(obj) {
    
    if (typeof obj === 'object' && obj !== {}){
        let values = [];

        for (data in obj) {
            values.push(obj[data]);
        }

        return values;

    } else {
        return [];
    }
         
}

module.exports = getValues;