function doInvert(obj) {

    if (typeof obj === 'object' && obj !== {}){
        let invertObj = {};

        for (data in obj) {
            invertObj[obj[data]] = data;
        }
    
        return invertObj;

    } else {
        return {};
    }
    
}

module.exports = doInvert;

