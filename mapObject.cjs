function mapObj(obj, func) {

    if (typeof obj === 'object' && obj !== {}){
        let newObj = {};

        for (data in obj) {
            newObj[data] = func(obj[data], data);
        }
    
        return newObj;

    } else {
        return {};
    }
    
}

module.exports = mapObj;